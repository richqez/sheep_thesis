<?php 

	function renderTemplate($viewName,$data){
		$CI =& get_instance();
		$CI->load->view('view_header');
		$CI->load->view($viewName,$data);
		$CI->load->view('view_footer');
	}	

	function renderToJson($data){
		header('Content-type: application/json;charset=utf-8');
    	echo json_encode( $data );
	}

 ?>
