<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		
	}

	public function index()
	{
		if (!isLogin()) {
				redirect('login');
		}		
	}

	public function mange(){
		renderTemplate('member_mange',[]);
	}

	public function getMembers(){
		$members = $this->db->get('member')->result();
		renderToJson(["data"=>$members]);
	}

	public function add_form(){
		renderTemplate('add_member',[]);
	}

	public function add(){
		$this->db->insert('member',["username" => $this->input->post("username"), 
								    "password" => $this->input->post("password"),
								    "role" => $this->input->post("role")
								  ]);
		renderTemplate('add_member',["result" => true]);
	}


	public function update_form($id){
		$member = $this->db->get_where("member",["id"=>$id])->row();
		renderTemplate('update_member',["member"=>$member]);
	}

	public function update(){

		$data = ["username"=>$this->input->post("username"),
				 "password"=>$this->input->post("password"),
				 "role"=>$this->input->post("role")];

		$this->db->update('member',$data,["id"=>$this->input->post("id")]);

		redirect('member/mange');
	}

	public function delete($id){
		$this->db->delete('member',["id"=>$id]);
		renderTemplate('member_mange',[]);
	}	

}

/* End of file Member.php */
/* Location: ./application/controllers/Member.php */