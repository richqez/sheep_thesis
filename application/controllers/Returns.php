<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Returns extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index(){
		if (isAdmin()) {
			renderTemplate('search_returns',[]);
		}else{

			
			  $keyword = getCurrentUser()->username;
			  $sql = " SELECT member.username as mem_username,member.id as mem_id , rent.id as rent_id , rent.date as rent_date, rent.expired as rent_expried
				       FROM member
				       INNER JOIN rent
				       ON rent.ref_member = member.id
				       WHERE member.username = '$keyword' ";
		       //var_dump($this->db->query($sql)->result());
				renderTemplate('search_returns',["search"=>$this->db->query($sql)->result(),"keyword"=>$keyword]);
		}
	}


	public function search(){

	  $keyword = $this->input->post('keyword');
	  $sql = " SELECT member.username as mem_username,member.id as mem_id , rent.id as rent_id , rent.date as rent_date, rent.expired as rent_expried
		       FROM member
		       INNER JOIN rent
		       ON rent.ref_member = member.id
		       WHERE member.username = '$keyword' ";
       //var_dump($this->db->query($sql)->result());
		renderTemplate('search_returns',["search"=>$this->db->query($sql)->result(),"keyword"=>$keyword]);
	}

	public function detail($id){
		$sql = "SELECT thesis.id,thesis.name ,thesis.year ,thesis.status
				FROM rent
				INNER JOIN rent_detail 
				ON rent_detail.ref_rent =rent.id
				INNER JOIN thesis
				ON rent_detail.ref_thesis = thesis.id
				WHERE rent.id = $id
				";
		 //var_dump();
		 renderTemplate('show_rendetail',["search"=>$this->db->query($sql)->result()]);
	}

	public function returned($id){
		$this->db->update('thesis',["status"=>"normal"],["id"=>$id]);
	}



}

/* End of file Return.php */
/* Location: ./application/controllers/Return.php */