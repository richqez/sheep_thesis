<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index(){
		$this->load->view('login');
	}


	public function login(){
		$query = $this->db->get_where("member",["username"=>$this->input->post("username")]);
		if (is_null($query->row())) {
			$this->load->view('login',["error"=>"username not found"]);
		}
		else{
			$row  = $query->row();
			if ($this->input->post("password") == $row->password) {
				
				$this->session->set_userdata('currentUser',$row);

				redirect('dash');

			}
			else{
				$this->load->view('login',["error"=>"password not match"]);
			}

		}
		
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login','refresh');
	}


}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */