<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rent extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		
	}

	public function add_form(){
		renderTemplate('add_rent',[]);
	}

	public function search(){
		$keyword = $this->input->post('keyword');

		if ($keyword!=='' and $this->input->post('keyword')!== null ) {
			$this->db->like('name',$keyword);
			$this->db->or_where('year',$keyword);
			$this->db->from('thesis');
			$result = $this->db->get()->result();
		}
		else{
			$result = [];
		}
		renderTemplate('add_rent',["search"=>$result]);
	}

	public function addto($id){

		$row = $this->db->get_where('thesis',["id"=>$id])->row();

		if (!$this->session->has_userdata('rent') || sizeof($this->session->userdata('rent')) == 0) {
			$this->session->set_userdata(["rent"=>array($row)]);
		}else{

			$rents = $this->session->userdata('rent');

			if (!in_array($row,$rents)) {
				array_push($rents,$row);
			}
			

			$this->session->set_userdata("rent",$rents);
		}



		renderTemplate('add_rent',[]);
		
	}

	public function removeto($id){
		$rents = $this->session->userdata('rent');
		foreach ($rents as $key => $value) {
			if ($value->id == $id) {
				unset($rents[$key]);
			}
		}
		$this->session->set_userdata("rent",$rents);
		renderTemplate('add_rent',[]);
	}

	public function checkout(){

		if ($this->session->has_userdata('rent') && sizeof($this->session->userdata('rent')) !== 0) {

			$user = $this->session->userdata('currentUser');

			$rents = $this->session->userdata('rent');


			$rent= [
					"total"      => sizeof($rents) , 
					"date"       => $date=date('Y-m-d') ,
					"ref_member" => $user->id,
					"expired"    => date('Y-m-d',strtotime("$date +7 day")) 
				    ] ;


			$this->db->insert('rent',$rent);

			$rent_id = $this->db->insert_id();


			foreach ($rents as $key => $value) {
				
				$rent_detail=
					[
						'ref_thesis' =>$value->id,
						'ref_rent'   => $rent_id
					];

				$this->db->insert('rent_detail',$rent_detail);
				$this->db->update('thesis', ["status"=>"hold"],["id"=>$value->id]);
			}

			$this->session->set_userdata("rent",[]);

			renderTemplate('add_rent',[]);

		}else{

			renderTemplate('add_rent',["error"=>"plase select Thesis !!!"]);

		}
	
	}


	

}

/* End of file Rent.php */
/* Location: ./application/controllers/Rent.php */