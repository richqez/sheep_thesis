<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thesis extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function mange()
	{
		renderTemplate('mange-thesis',[]);
	}


	public function getThesis(){
		$thesis =  $this->db->get('thesis')->result();
		renderToJson(["data" => $thesis]);
	}

	public function add_form(){
		renderTemplate('add_thesis',[]);
	}

	public function add(){
		
		$data = ["name"=>$this->input->post("name"),
				 "detail" => $this->input->post("detail"),
				 "year"=>$this->input->post("year")
				];
		
		$this->db->insert('thesis',$data);

		renderTemplate('add_thesis',["result"=>true]);
	}

	public function update_form($id){
		$thesis = $this->db->get_where("thesis",["id"=>$id])->row();	
		// var_dump($thesis);
		renderTemplate('update_thesis',["thesis"=>$thesis]);
	}

	public function update(){
		$data = ["name"=>$this->input->post("name"),
				 "detail" => $this->input->post("detail"),
				 "year"=>$this->input->post("year")
				];
		$this->db->update('thesis',$data,["id"=>$this->input->post("id")]);
		redirect('thesis/mange');
	}

	public function delete($id){
		$this->db->delete('thesis',["id"=>$id]);
		redirect('thesis/mange');
	}



}

/* End of file Thesis.php */
/* Location: ./application/controllers/Thesis.php */