<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dash extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (!isLogin()) {
			redirect('login');
		}
	}

	public function index()
	{	

		renderTemplate('search_returns',["user"=>$this->session->userdata('currentUser')]);

	}

}

/* End of file Dash.php */
/* Location: ./application/controllers/Dash.php */