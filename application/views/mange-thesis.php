  <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Thesis <small>CRUD Thesis </small>
                        </h1>
                    
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-book"></i> Manage > thesis 
                            </li>
                        </ol>
                    </div>
                </div>
               <div class="panel panel-green">
                    <div class="panel-heading">
                             <h3 class="panel-title">
                             <i class="fa fa-table"></i> Data Thesis
                             </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table" id="tbl_thesis">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Year</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-bottom:10px">
                    <div class="col-lg-12">
                             <a style="padding-buttom:10px" href="<?= base_url()?>thesis/add_form" type="button" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span> AddThesis
                        </a>
                    </div>
                </div>

                <script>
                  (function(){

                    var $tbl_thesis = $('#tbl_thesis');

                    var oTbl_thesis = $tbl_thesis.DataTable({
                        ajax      : "<?= base_url()?>thesis/getThesis",
                        pageLength: 10,
                        columns   : [
                            {data : "name"},
                            {data : "year"},
                            {data : "status"},
                            {mRender: function(data,type,full){
                                return '<a deletethesis href="<?= base_url()?>thesis/delete/'+full.id+' " type="button" class="btn btn-danger">'
                                 +'<span class="glyphicon glyphicon-trash"/></a>&nbsp;'
                                 +'<a href="<?= base_url() ?>/thesis/update_form/'+full.id+'" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" /></a>';
                            }}
                        ]
                     });

                    
                    $tbl_thesis.on('click', 'a[deletethesis]', function(event) {
                        var isConfrim = confirm("Are you want to delete");
                        if (!isConfrim) {
                            event.preventDefault();
                        };
                    });


                  })()
                </script>