 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Thesis
                            <small> ADD THESIS </small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-book"> Manage > thesis  > addThesis</i> 
                            </li>
                        </ol>
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                     <h3 class="panel-title">
                                     <i class="fa fa-book"></i> Add Thesis
                                     </h3>
                            </div>
                            <div class="panel-body">
                                 <form role="form" action="<?= base_url() ?>thesis/add" method="post">

                                    <div class="col-md-4"> 
                                        <?php if (isset($result)): ?>
                                            <?php if ($result): ?>
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <strong>Success</strong> insert Thesis success
                                                </div>
                                            <?php endif ?>
                                        <?php endif ?>
                                    <div class="form-group">
                                            <label>Name </label>
                                            <input class="form-control" name="name">

                                            <label>Detail </label>
                                            <textarea class="form-control" rows="3" name="detail">
                                                

                                            </textarea>

                                            <label>Year </label>
                                                <div class='input-group date' id='datetimepicker2' >
                                                    <input type='text' class="form-control" name="year" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                    </div>

                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <!-- <button type="submit" class="btn btn-default">Reset</button> -->
                                        <a href="<?php echo base_url() ?>thesis/mange" class="btn btn-danger">Back</a>
                                       

                                    </div>

                                 </form>
                            </div>
                        </div>
                    </div>
                </div>

          
       
