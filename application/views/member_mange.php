  <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Member <small>CRUD Member</small>
                        
                        </h1>
                       
                        
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Manage > member
                            </li>
                        </ol>
                    </div>
                </div>
                 <div class="panel panel-green">
                    <div class="panel-heading">
                             <h3 class="panel-title">
                             <i class="fa fa-table"></i> Data Member
                             </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table" id="tbl_member">
                                    <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Password</th>
                                            <th>Role</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-bottom:10px">
                    <div class="col-lg-12">
                        <a style="padding-buttom:10px" href="<?= base_url()?>member/add_form" type="button" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span> AddMember
                        </a>    
                    </div>
                </div>
                <script>
                  (function(){

                    var $tbl_member = $('#tbl_member');

                    var oTbl_member = $tbl_member.DataTable({
                        ajax      : "<?= base_url()?>member/getMembers",
                        pageLength: 10,
                        columns   : [
                            {data : "username"},
                            {data : "password"},
                            {data : "role"},
                            {mRender: function(data,type,full){
                                return '<a deletemember href="<?= base_url()?>member/delete/'+full.id+' " type="button" class="btn btn-danger">'
                                 +'<span class="glyphicon glyphicon-trash"/></a>&nbsp;'
                                 +'<a href="<?= base_url() ?>member/update_form/'+full.id+'" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" /></a>';
                            }}
                        ]
                     });

                    
                    $tbl_member.on('click', 'a[deletemember]', function(event) {
                        var isConfrim = confirm("Are you want to delete");
                        if (!isConfrim) {
                            event.preventDefault();
                        };
                    });


                  })()
                </script>