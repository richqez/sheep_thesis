 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Thesis
                            <small> UPDATE THESIS</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                               <i class="fa fa-book"> Manage > thesis  > updateThesis</i> 
                            </li>
                        </ol>
                         <div class="panel panel-green">
                            <div class="panel-heading">
                                     <h3 class="panel-title">
                                     <i class="fa fa-book"></i> Add Thesis
                                     </h3>
                            </div>
                            <div class="panel-body">
                                 <form role="form" action="<?= base_url() ?>thesis/update" method="post">

                                    <div class="col-md-4"> 
                                        <?php if (isset($result)): ?>
                                            <?php if ($result): ?>
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <strong>Success</strong> insert Thesis success
                                                </div>
                                            <?php endif ?>
                                        <?php endif ?>
                                    <div class="form-group">
                                            <input name="id" value="<?= $thesis->id ?>" type="hidden">
                                            <label>Name :</label>
                                            <input class="form-control" name="name" value="<?= $thesis->name ?>">

                                            <label>Detail :</label>
                                            <textarea class="form-control" rows="3" name="detail" value="<?= $thesis->detail ?>">
                                                

                                            </textarea>

                                            <label>Year :</label>
                                                <div class='input-group date' id='datetimepicker2' >
                                                    <input type='text' class="form-control" name="year"  value="<?= $thesis->year ?>" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                    </div>

                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <a href="<?php echo base_url() ?>thesis/mange" class="btn btn-danger">Back</a>
                                        <!-- <button type="submit" class="btn btn-default">Reset</button> -->
                                       

                                    </div>

                                 </form>
                            </div>
                        </div>
                    </div>
                </div>

          
       
