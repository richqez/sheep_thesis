<div class="row">
  <div class="col-lg-12">
                        <h1 class="page-header">
                        	Manage Thesis
                            <small> Returns </small>
                        </h1>
                        <ol class="breadcrumb">
				            <li class="active">
				                <i class="fa fa-refresh"> Manage > thesis > return > manage return </i> 
				            </li>
				        </ol>
     </div>
</div>
<div class="panel panel-green">
    <div class="panel-heading">
        <h3 class="panel-title">
        <i class="fa fa-book"></i> Returns Thesis
        </h3>
    </div>
    <div class="panel-body">
		<div class="row" align="center">

					<div class="col-md-8 col-md-offset-2">
						<table class="table table-bordered table-hover" id="return_tbl">
							<tr>
								<th class="col-md-8" align="center">name</th>
								<th class="col-md-2">year</th>
								<th class="col-md-2"></th>
							</tr>
							<?php foreach ($search as $key => $value): ?>
								<tr>
									<td><?= $value->name ?></td>
									<td><?= $value->year ?></td>
									<td>
										<?php if ($value->status == 'normal'): ?>
											<span class="label label-success">Returned</span>
											<?php else: ?>
												<button type="button" class="btn btn-info" data-key="<?= $value->id ?>">Return</button>
										<?php endif ?>

									</td>
								</tr>
								<tr></tr>
							<?php endforeach ?>
						</table>
					</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url() ?>returns" class="btn btn-danger">Back</a>
		<!-- <button type="button" class="btn btn-info">back</button> -->
	</div>
</div>
<script>
	jQuery(document).ready(function($) {
			$('#return_tbl')
				.on('click', 'button', function(event) {
					event.preventDefault();
					var key = $(this).data('key') ;
					$.get('<?= base_url() ?>returns/returned/'+key, function(data) {
						location.reload();
					});
				});
	});
</script>