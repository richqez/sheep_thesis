 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Member
                            <small> UPDATE MEMBER </small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-user"> Manage > member > updateMember </i> 
                            </li>
                        </ol>
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                     <h3 class="panel-title">
                                     <i class="fa fa-user"></i> Update Member
                                     </h3>
                            </div>
                            <div class="panel-body">
                                 <form role="form" action="<?= base_url() ?>member/update" method="post">

                                    <div class="col-md-4"> 

                                        <?php if (isset($result)): ?>
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <strong>Success</strong> add member success
                                            </div>
                                        <?php endif ?>

                                        <input type="hidden" value="<?= $member->id ?>" name="id">

                                         <div class="form-group">
                                            <label>UserName :</label>
                                            <input class="form-control" name="username" value="<?= $member->username?>">

                                            <label>Password :</label>
                                            <input class="form-control" name="password" value="<?= $member->password ?>">

                                             <label>Role</label>
                                             <select class="form-control" name="role">
                                                 <option value="admin"
                                                    <?php if ($member->role == 'admin'): ?>
                                                        selected
                                                    <?php endif ?>

                                                 >Admin</option>
                                                 <option value="user"
                                                    <?php if ($member->role == 'user'): ?>
                                                        selected
                                                    <?php endif ?>
                                                 >User</option>
                                             </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <a href="<?php echo base_url() ?>member/mange" class="btn btn-danger">Back</a>
                                        <!-- <button type="reset" class="btn btn-default">Reset</button> -->                                   
                                    </div>
                                 </form>
                            </div>
                        </div>
                    </div>
                </div>

          
       
