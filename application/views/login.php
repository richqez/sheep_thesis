<!DOCTYPE html>
<html>
	<head>
		<title>Login Thesis</title>
		 <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
		  <link href="<?php echo base_url(); ?>public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		  <style type="text/css">
		  	body{
		  	  background-image: url("<?php echo base_url(); ?>public/images/login_bg.jpg");
		  	}
		  	/* Credit to bootsnipp.com for the css for the color graph */
			.colorgraph {
			  height: 5px;
			  border-top: 0;
			  background: #c4e17f;
			  border-radius: 5px;
			  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
			  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
			  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
			  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
			}
			#box_tranparent{
				border-radius: 25px;
				background-color : transparent;
				background-color: rgba(192,192,192, 0.4);
				-webkit-box-shadow: 12 8px 6px -6px black;
			    -moz-box-shadow: 0 8px 6px -6px black;
			        box-shadow: 0 8px 6px -6px black;
			}
		  </style>
	</head>
	<body>
		 <div class="container">

			<div class="row" style="margin-top:8%;background-color: ;" >
				<div class="col-sm-2 col-md-3 "></div>
			    <div class="col-xs-12 col-sm-8 col-md-6 " id="box_tranparent">
			    	
					<form role="form" action="<?= base_url() ?>login/login" method="post">
						<fieldset>
							<h2 style="color:black">Please Sign In Thesis</h2>
							<hr class="colorgraph">
							<div class="form-group">
			                    <input type="text" name="username" id="email" class="form-control input-lg" placeholder="username">
							</div>
							<div class="form-group">
			                    <input type="password" name="password" id="password" class="form-control input-lg" placeholder="password">
							</div>
							<?php if (isset($error)): ?>
					    		<div class="alert alert-danger">
						    		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						    		<strong>Oops!</strong> <?= $error ?>
						    	</div>
					    	<?php endif ?>
							<span class="button-checkbox">
								<button type="button" class="btn" data-color="info">Remember Me</button>
			                    <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
								<!-- <a href="" class="btn btn-link pull-right">Forgot Password?</a> -->
							</span>							
							<hr class="colorgraph">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12" style="margin-bottom:5%">
			                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign In">
								</div>
								<!-- <div class="col-xs-6 col-sm-6 col-md-6">
									<a href="" class="btn btn-lg btn-primary btn-block">Register</a>
								</div> -->
							</div>
						</fieldset>
					</form>
				</div>
			</div>

</div>
	</body>
</html>

  <!-- jQuery -->
    <script src="<?php echo base_url(); ?>public/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->


<script type="text/javascript">
	
$(function(){
    $('.button-checkbox').each(function(){
		var $widget = $(this),
			$button = $widget.find('button'),
			$checkbox = $widget.find('input:checkbox'),
			color = $button.data('color'),
			settings = {
					on: {
						icon: 'glyphicon glyphicon-check'
					},
					off: {
						icon: 'glyphicon glyphicon-unchecked'
					}
			};

		$button.on('click', function () {
			$checkbox.prop('checked', !$checkbox.is(':checked'));
			$checkbox.triggerHandler('change');
			updateDisplay();
		});

		$checkbox.on('change', function () {
			updateDisplay();
		});

		function updateDisplay() {
			var isChecked = $checkbox.is(':checked');
			// Set the button's state
			$button.data('state', (isChecked) ? "on" : "off");

			// Set the button's icon
			$button.find('.state-icon')
				.removeClass()
				.addClass('state-icon ' + settings[$button.data('state')].icon);

			// Update the button's color
			if (isChecked) {
				$button
					.removeClass('btn-default')
					.addClass('btn-' + color + ' active');
			}
			else
			{
				$button
					.removeClass('btn-' + color + ' active')
					.addClass('btn-default');
			}
		}
		function init() {
			updateDisplay();
			// Inject the icon if applicable
			if ($button.find('.state-icon').length == 0) {
				$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
			}
		}
		init();
	});
});

</script>