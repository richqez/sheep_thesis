<div class="row" >
	<div class="col-lg-12">
		<h1 class="page-header">
            Manage Thesis	
			<small>Return Thesis Record</small>
		</h1>
		<ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-refresh"> Manage > thesis > return </i> 
            </li>
        </ol>
	</div>
</div>
<div class="row" align="center">
	<div class="col-lg-6 col-lg-offset-3">
		 <?php if (isAdmin()): ?>
		 	<form class="form-inline" action="<?= base_url() ?>returns/search" method="post" >
		       <div class="form-group">
		         <label for="exampleInputName2">UserName</label>
		         <input type="text" class="form-control" name="keyword">
		       </div>
		    	<button type="submit" class="btn btn-default">Search</button>
		     </form>
		 <?php endif ?>
	</div>
</div>

<div class="row" style="padding-top:20px" align="center">
<?php if (isset($search)): ?>
	<?php if (sizeof($search) < 1): ?>
	
		<div class="col-lg-6 col-lg-offset-3">
			<h3>Not Found RentDate from <b>" <?= $keyword ?> "</b> in database plase try agian</h3>
		</div>

	<?php else: ?>
			<div class="col-md-8 col-md-offset-2">
				<table class="table table-bordered table-hover">
					<tr>
						<th class="col-md-10" align="center">Rent Date</th>
						<?php if ( isAdmin() ): ?>
								<th class="col-md-2"></th>
							<?php else: ?>
								
							<?php endif ?>
						
					</tr>
					<?php foreach ($search as $key => $value): ?>
						<tr align="center">
							<td><?= $value->rent_date ?></td>
							<?php if ( isAdmin() ): ?>
								<td><a href="<?= base_url()?>returns/detail/<?= $value->rent_id ?>" class="btn btn-info">Select</a></td>
							<?php else: ?>
								
							<?php endif ?>
						</tr>
						<tr></tr>
					<?php endforeach ?>
				</table>
			</div>
	<?php endif ?>
<?php endif ?>
	</div>