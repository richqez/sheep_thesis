 <!-- Page Heading -->
                <div class="row" >
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Manage Thesis
                            <small>Rent Thesis Record</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-database"> Manage > thesis > rent </i> 
                            </li>
                        </ol>
                        <?php if (isset($error)): ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Somthing wrong</strong> <?= $error ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-6 col-md-offset-3" >
                        <div class="row" align="center" style="padding-bottom:10px">
                            <div class="col-md-12">
                                <form class="form-inline" action="<?= base_url() ?>rent/search" method="post" >
                                  <div class="form-group">
                                    <label for="exampleInputName2">Keyword</label>
                                    <input type="text" class="form-control" placeholder="ex. name,year" name="keyword">
                                  </div>
                                  <button type="submit" class="btn btn-default">Search</button>
                                </form>
                            </div>
                        </div>
                        <?php if (isset($search)): ?>
                            <?php if (sizeof($search) > 0 ): ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-bordered table-hover">
                                            <tr>
                                                <th>#</th>
                                                <th>name</th>
                                                <th>year</th>
                                                <th>status</th>
                                                <th></th>
                                            </tr>
                                            <?php foreach ($search as $key => $value): ?>
                                                <tr>
                                                    <td><?= $key+1 ?></td>
                                                    <td><?= $value->name ?></td>
                                                    <td><?= $value->year ?></td>
                                                    <td><?= $value->status ?></td>
                                                    <td align="center">
                                                       <?php if ($value->status !== 'hold'): ?>
                                                            <a href="<?= base_url() ?>rent/addto/<?= $value->id?>" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></a>
                                                       <?php endif ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </table>
                                    </div>
                               </div>
                            <?php else: ?>
                                <div class="row" align="center">
                                    <h3>Not Found Data</h3>
                                </div>
                            <?php endif ?>
                        <?php endif ?>

                    </div>
                </div>


                <div class="row">
                   <div class="panel panel-default">
                       <div class="panel-body">
                            <div class="col-md-4">
                                    <form role="form" class="form-horizontal">
                                        <div class="form-group">
                                            <a href="<?= base_url()?>rent/checkout" class="btn btn-default btn-lg">checkout</a>
                                        </div>
                                       <div class="form-group">
                                        <label class="col-sm-2 control-label">Date </label>
                                        <div class="col-sm-10">
                                          <input disabled  type="text" class="form-control" value="<?= $date = date('d-m-Y') ?>" >
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">Expried </label>
                                        <div class="col-sm-10">
                                          <input disabled  type="text" class="form-control"  value="<?= date('d-m-Y',strtotime("$date +7 day")) ?>">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label">RentBy </label>
                                        <div class="col-sm-10">
                                          <input disabled type="text" class="form-control"  value="<?= $this->session->userdata('currentUser')->username; ?>">
                                        </div>
                                      </div>
                                 </form>
                            </div>

                            <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Year</th>
                                                    <th></th>
                                                </tr>
                                               <?php if ($this->session->has_userdata('rent')): ?>
                                                    <?php $rents = $this->session->userdata('rent'); ?>
                                                    <?php foreach ($rents as $key => $value): ?>
                                                        <tr>
                                                            <td><?= $value->name ?></td>
                                                            <td><?= $value->year ?></td>
                                                            <td align="center">
                                                                <a href="<?= base_url() ?>rent/removeto/<?= $value->id ?>" class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></a>

                                                            </td>
                                                        </tr>
                                                    <?php endforeach ?>
                                               <?php endif ?>
                                            </thead>
                                            </table>
                                        </div>

                            </div>
                       </div>
                   </div>
                </div>
       
              