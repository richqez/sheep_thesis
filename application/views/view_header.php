<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Thesis</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>public/css/datatables.min.css"/>
    
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>public/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url(); ?>public/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.<?php echo base_url(); ?> public/js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo base_url(); ?>public/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>

    <script src="<?= base_url() ?>public/js/datatables.min.js"></script>

</head>

<body >

    <div id="wrapper" >

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Sheep Management Thesis </a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav" >
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= getCurrentUser()->username ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?= base_url() ?>login/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse" >
                <ul class="nav navbar-nav side-nav" >
                    <!-- <li class="active">
                        <a href="<?= base_url() ?>dash"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li> -->
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#thesis"><i class="glyphicon glyphicon-book"></i> Thesis <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="thesis" class="collapse">
                            <?php if (isAdmin()): ?>
                                <li>
                                    <a href="<?= base_url() ?>thesis/mange">Manage Thesis</a>
                                </li>
                            <?php endif ?>
                            <li>
                                <a href="<?= base_url() ?>rent/add_form">Rent Thesis</a>
                            </li>
                            <?php if (isAdmin()): ?>
                                <li>
                                    <a href="<?= base_url() ?>returns">Return Thesis</a>
                                </li>
                            <?php else : ?>
                                 <li>
                                    <a href="<?= base_url() ?>returns">Loan Date</a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </li>
                    <?php if (isAdmin()): ?>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#member"><i class="glyphicon glyphicon-user"></i> Member <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="member" class="collapse">
                                <li>
                                    <a href="<?= base_url() ?>member/mange">Manage Member</a>
                                </li>
                            </ul>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper" >

            <div class="container-fluid">

               <!-- Content HERE -->
           
